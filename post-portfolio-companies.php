<?php if(!is_single()){ ?>
	
	<script type="text/javascript">
	$('#slideCl').on('click', function(e){
    e.preventDefault();
    $("#slideOut").removeClass('open');
    $("#slideOut .port").html('');
    $('body').removeClass('overlain');
    $('html').removeClass('overlain');
    $("#nav-overlay").removeClass('open');

  });
  </script>
<?php
} ?>
<div class="portfolio-member">
			<?php 
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
			$thumb_url = $thumb_url_array[0];
			$theID = get_the_ID();
			?>
			<img class="featured" src="<?php echo $thumb_url; ?>" alt="" />
			
			<div class="flex">
				<div class="flexBox flexLeft">	
					<span class="key">Sector</span><span class="value"><?php echo get_field('sector'); ?></span>
					<span class="key">Invested</span><span class="value"><?php echo get_field('invested'); ?></span>
					<span class="key">Headquarters</span><span class="value"><?php echo get_field('headquarters'); ?></span>
					<span class="key">Status</span><span class="value"><?php echo get_field('status'); ?></span>
				</div>
				<div class="flexBox flexRight">
					<img class="portfolio-logo" src="<?php echo get_field('logo'); ?>" width="280" alt="" />
				</div>
			</div>

			<div class="intro">
				<p class="intro"><?php echo get_field('intro_heading'); ?></p>
				<p class="secondary"><?php echo get_field('intro_text'); ?></p>

				<section class="social">
					<?php $socials = get_field('links');
						foreach ($socials as $socialLink) {
							
						switch($socialLink['link_type']){

							case 'web':
								$link = "<a href='".$socialLink['url']."' class='nobg'><img class='globe' src='".get_bloginfo('template_directory')."/images/globe.svg' height='30' width='30' /></a>";
								break;
							case 'tw':
								$link = "<a rel='external' href='".$socialLink['url']."'><i class='fa fa-twitter'></i></a>";
								break;
							case 'li':
								$link = "<a rel='external' href='".$socialLink['url']."'><i class='fa fa-linkedin'></i></a>";
								break;
							case 'fb':
								$link = "<a rel='external' href='".$socialLink['url']."'><i class='fa fa-facebook'></i></a>";
								break;
						}
						echo $link;
					}
					?>
					<!--<a href="#" class="nobg"><img class="globe" src="images/globe.svg" height="30" width="30" /></a>
					<a rel="external" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//dev.zoyeswebsites.com/rockbridge/portfolio.php"><i class="fa fa-facebook"></i></a>
					<a rel="external" href="https://twitter.com/home?status=http%3A//dev.zoyeswebsites.com/rockbridge/portfolio.php"><i class="fa fa-twitter"></i></a>-->
				</section>
			</div>
			<section class="testCar">
				<section class="wrapper">
					<?php $testi = get_field('testimonial');

					?>
					<p class="quote"><?php echo get_field('testimonial', $testi); ?></p>
					<p class="att">
						<span class="name"><?php echo get_field('author', $testi); ?>, <?php echo get_field('title', $testi); ?></span>
						<span class="company"><?php echo get_field('company', $testi); ?></span>
					</p>
				</section>
			</section>
			<section class="thesis">
				<?php the_content(); ?>
			</section>
			<section class="navigation">
				<div class="prev"><a href="#" class="get_pc" data-postid="<?php echo get_previous_post_id( $theID ); ?>"><i class="fa fa-long-arrow-left"></i></a></div>
				<div class="all"><a href="/rge/portfolio/">View All Partnerships</a></div>
				<div class="next"><a href="#" class="get_pc" data-postid="<?php echo get_next_post_id( $theID ); ?>"><i class="fa fa-long-arrow-right"></i></a></div>
			</section>
		</div>