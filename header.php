<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<script src="https://use.fontawesome.com/d794afba64.js"></script>
		<script src="https://use.typekit.net/vvr0esj.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<meta name="keywords" content="" />
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" />
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>
		<div id="preloader">
    		<div id="status">&nbsp;</div>
		</div>

	<header id="nav-top" class="exp<?php if(is_page_template('template-contact.php')){echo ' small'; }?>">
		<span class="blocker"></span>
		<!--<ul class="lvl1">
			<li><a href="#">Approach</a></li>
			<li><a href="#">Investment Criteria</a></li>
			<li><a href="portfolio.php">Portfolio</a></li>
			<li><a href="#">Team</a></li>
		</ul>-->
		<?php html5blank_nav('top-menu'); ?>
		<button id="navBtn">Menu</button>
	</header>
	<h1 id="logoFt"><a href="<?php echo home_url(); ?>">Rockbridge Growth Equity</a></h1>
	<section id="nav-overlay"></section>
	<section id="nav-full" class="">
		<button id="navCl">Close</button>
		<ul class="lvl2">
			<!--<li><a href="#">Approach</a></li>
			<li><a href="#">Investment Criteria</a></li>
			<li><a href="portfolio.php">Portfolio</a></li>
			<li><a href="#">Team</a></li>
			<li class="pad"><a href="#">Family of Companies</a></li>
			<li><a href="#">Press Room</a></li>
			<li><a href="#">Contact</a></li>-->
			<?php wp_nav_menu('full-menu'); ?>
			<li class="pad">
				<form id="search" action="#">
					<input type="text" required id="searchInput" value="" />
					<label for="searchInput"><i class="fa fa-search"></i></label>
				</form>
			</li>
		</ul>
		<article class="footer">
			<span class="ph"><i class="fa fa-phone"></i>313.373.7000</span>
			<span class="em"><a href="mailto:info@rbequity.com"><i class="fa fa-envelope"></i><span>info@rbequity.com</span></a></span>
			<a href="#" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
		</article>
	</section>