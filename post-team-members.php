<?php if(!is_single()){ ?>
	<script type="text/javascript">
	$('#slideCl').on('click', function(e){
    e.preventDefault();
    $("#slideOut").removeClass('open');
    $("#slideOut .port").html('');
    $('body').removeClass('overlain');
    $('html').removeClass('overlain');
    $("#nav-overlay").removeClass('open');
  });
  </script>
<?php
} ?>
<div class="team-member">
	<?php 
	$thumb_url = get_field('full_size_image');
	$theID = get_the_ID();
	?>
	<img class="featured" src="<?php echo $thumb_url; ?>" alt="" />
	
	<div class="flex">
		<div class="flexBox flexLeft">	
			<h4><?php echo get_field('title'); ?> &mdash;</h4>
			<h2><?php echo get_the_title(); ?></h2>
		</div>
		<div class="flexBox flexRight">
			<span><a href="<?php echo get_field('linkedin_url'); ?>"><i class="fa fa-linkedin"></i></a></span>
			<span><a href="<?php echo get_field('email'); ?>"><i class="fa fa-envelope"></i></a></span>
		</div>
	</div>

	<div class="bio">
		<?php $bios = get_field('biography');
				foreach ($bios as $bioSection) {
					echo "<h3>".$bioSection['heading']."</h3>";
					echo "<p>".$bioSection['text']."</p>";			
			}
			?>					
	</div>			
	<section class="navigation">
		<div class="prev"><a href="#" class="get_pc" data-postid="<?php echo get_previous_post_id( $theID ); ?>"><i class="fa fa-long-arrow-left"></i></a></div>
		<div class="all"><a href="/the-power-of-our-team/">View All Team Members</a></div>
		<div class="next"><a href="#" class="get_pc"><i class="fa fa-long-arrow-right"></i></a></div>
	</section>
</div>