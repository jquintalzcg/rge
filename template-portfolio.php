<?php /* Template Name: Portfolio Page Template */ get_header(); 
	$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
			$thumb_url = $thumb_url_array[0]; ?>
	<section id="portHero" class="hero block" style="background-image:url('<?php echo $thumb_url; ?>');">
		<a href="/rockbridge/" id="logo" class="main">Rockbridge Growth Equity</a>
		<h1><?php echo get_field('hero_heading'); ?></h1>
	</section>
	<section class="intro block">
		<article class="contain cols">
			<div class="row colFlex">
				<h3 class="span5"><?php echo get_field('intro_heading'); ?></h3>
				<div class="span7">
					<p class="intro"><?php echo get_field('intro_text'); ?></p>
					<p class="secondary"><?php echo get_field('intro_cta'); ?></p>
				</div>
			</div>
		</article>
	</section>
	<section id="features" class="block">
		<?php
			$feature1 = get_field('featured_portfolio_company_1');
			$feature2 = get_field('featured_portfolio_company_2');
			$feat1 = $feature1;
			$feat2 = $feature2;
			?>
		<article class="feat feat1">			
			<div class="bg" style="background-image:url('<?php echo get_field('feature_image', $feat1); ?>');"></div>
			<a href="#" class="cont get_pc" data-postid="<?php echo $feature1; ?>">
				<img src="<?php echo get_field('white_logo', $feat1); ?>" />
				<span class="more"><span>Learn More</span><span class="tri"></span></span>
			</a>
		</article>
		<article class="feat feat2">
			<div class="bg" style="background-image:url('<?php echo get_field('feature_image', $feat2); ?>');"></div>
			<a href="#" class="cont get_pc" data-postid="<?php echo $feature2; ?>">
				<img src="<?php echo get_field('white_logo', $feat2); ?>" />
				<span class="more"><span>Learn More</span><span class="tri"></span></span>
			</a>
		</article>
	</section>
	<section class="block" id="invGrid">
		<h2><?php echo get_field('portfolio_heading'); ?></h2>
		
		<ul class="contain">
			<?php
						// Get the 'Plans' post type
						$args = array(
						    'post_type' => 'portfolio_companies',
						    'posts_per_page' => -1,
						    'order' => 'ASC'
						);
						$loop1 = new WP_Query($args);

						while($loop1->have_posts()): $loop1->the_post(); ?>
							<li>
								<span class="logo tall"><img src="<?php echo get_field('logo'); ?>" /></span>
								<div>
									<span class="status">Status: <?php echo get_field('status'); ?></span>
									<span class="name"><?php the_title(); ?></span>
									<p><?php echo get_field('mini_bio'); ?></p>
									<a href="#" class="more get_pc" data-postid="<? echo get_the_ID(); ?>" ><span>Learn More</span><span class="tri"></span></a>
								</div>
							</li>
						<?php endwhile;
						wp_reset_query();
						?>
			
		</ul>
	</section>
	<section class="testimonial-wrapper">
		<section id="testCar" class="block cycle-slideshow"  
		    data-cycle-timeout="7000"
		    data-cycle-slides="> article"
		    data-cycle-pager=".testiPag"
		    data-cycle-pager-template="<li><a href=#> 0{{slideNum}} </a></li>"
		    data-cycle-fx="scrollVert"
		    data-cycle-auto-height="calc">

			<article class="contain">
				<p class="quote">"The Rockbridge partnership has allowed Connect America to achieve unparalleled success in the industry by helping to create a truly scalable and technology-enabled operational infrastructure"</p>
				<p class="att">
					<span class="name">Ken Gross, Founder &amp; Executive Chairman</span>
					<span class="company">Connect America</span>
				</p>
			</article>
			<article class="contain">
				<p class="quote">"The Rockbridge team brought the resources and capabilities to transform One Reverse into the largest company in the reverse mortgage space. The exceptional growth of our company would not have come to fruition without Rockbridge’s sponsorship."</p>
				<p class="att">
					<span class="name">Gregg Smith, President &amp; COO</span>
					<span class="company">One Reverse Mortgage</span>
				</p>
			</article>
			<article class="contain">
				<p class="quote">"Rockbridge Growth Equity has been a valuable strategic partner for Triad Retail Media. Throughout this partnership, we have been able to accelerate our growth, expand internationally, and position Triad as the global leader in digital retail media."</p>
				<p class="att">
					<span class="name">Roger Berdusco, CEO</span>
					<span class="company">Triad Retail Media</span>
				</p>
			</article>
			<ul class="pag testiPag">
				<!--<li class="active"><a href="#">01</a></li>
				<li><a href="#">02</a></li>
				<li><a href="#">03</a></li>-->
			</ul>
		</section>
	</section>
	
	
	<div id="slideOut">
		<header id="slide-top">
			<button id="slideCl">Close</button>
		</header>
		<div id="ajaxLoader">
			<div class="ajaxstatus">
			</div>
		</div>
		<div class="port">
		</div>
	</div>
<?php get_footer(); ?>
