jQuery(document).ready(function() {
	
	jQuery(".get_pc").on( 'click', function(e){
		e.preventDefault(); 
		var postid = jQuery(this).attr('data-postid');
	   jQuery( "#ajaxLoader" ).show();
		var adminUrl = '../wp-admin/admin-ajax.php';
		jQuery.ajax({
		type: 'POST',
		url: '../wp-admin/admin-ajax.php',
		data: {
			action: 'my_load_ajax_portfolio',
			postid: postid
			},
		success: function(data, textStatus, XMLHttpRequest){
			jQuery("#slideOut .port").html('');
			jQuery("#slideOut .port").append(data);
			jQuery('body').addClass('overlain');
			
		      jQuery('html').addClass('overlain');
		      jQuery('#slideOut').addClass('open');
		      jQuery("#nav-overlay").addClass('open');
			/*alert(data);
			alert(textStatus);
			alert(XMLHttpRequest);*/
			},
		error: function(MLHttpRequest, textStatus, errorThrown){
			alert(errorThrown);
			},
		complete: function(){
			jQuery('#ajaxLoader').fadeOut();
		}
		});
	});

	jQuery(".get_team").click(function(e){
		e.preventDefault(); 
		var postid = jQuery(this).attr('data-postid');      
  		jQuery( "#ajaxLoader" ).show();
		var adminUrl = '../wp-admin/admin-ajax.php';
		jQuery.ajax({
		type: 'POST',
		url: '../wp-admin/admin-ajax.php',
		data: {
			action: 'my_load_ajax_team',
			postid: postid
			},
		success: function(data, textStatus, XMLHttpRequest){
			jQuery("#slideOut .port").html('');
			jQuery("#slideOut .port").append(data);
			jQuery('body').addClass('overlain');
		      jQuery('html').addClass('overlain');
		      jQuery('#slideOut').addClass('open');
		      jQuery("#nav-overlay").addClass('open');
			/*alert(data);
			alert(textStatus);
			alert(XMLHttpRequest);*/
			},
		error: function(MLHttpRequest, textStatus, errorThrown){
			alert(errorThrown);
			},
		complete: function(){
			jQuery('#ajaxLoader').fadeOut();
		}
		});
	});
	});

jQuery( document ).ajaxStart(function() {
    
 });

 jQuery( document ).ajaxComplete(function() {
    
 });