	<?php if (!is_page_template('template-contact.php')){ ?>
	<section id="cta" class="block">
		<article class="contain">
			<h2>Let's Build <br />Something Great</h2>
			<a href="#">Contact Rockbridge</a>
		</article>
	</section>
	<?php } ?>
	<footer>
		<article class="contact">
			<span class="ph"><i class="fa fa-phone"></i>313.373.7000</span>
			<span class="em"><a href="mailto:info@rbequity.com"><i class="fa fa-envelope"></i><span>info@rbequity.com</span></a></span>
		</article>
		<p>&copy; <?php echo date('Y'); ?> <?php echo bloginfo('name'); ?> <span>All Rights Reserved.</span></p>
	</footer>
	<?php wp_footer(); ?>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fastclick.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/retina.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.scrollVert.js"></script>

	<script type="text/javascript">
		$(window).scroll(function () {
		   $('#landing .more').toggleClass("faded", ($(window).scrollTop() > 1));
		});
	</script>
	
</body>

</html>