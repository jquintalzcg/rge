<?php /* Template Name: Home Page Template */ get_header(); ?>

	<section id="landing" class="block">
		<a href="/rockbridge/" id="logo" class="main">Rockbridge Growth Equity</a>
		<article>
			<h1><?php echo get_field('hero_heading'); ?></h1>
			<h3><?php echo get_field('hero_text'); ?></h3>
		</article>
		<a href="#homeIntro" class="more continue"><span>Continue</span><span class="tri"></span></a>
	</section>
	<section id="homeIntro" class="block">
		<article class="contain">
			<img src="<?php echo get_field('approach_bg'); ?>" alt="Rockbridge Growth Equity" />
			<div class="cont">
				<p class="intro"><?php echo get_field('approach_heading'); ?></p>
				<p class="secondary"><?php echo get_field('approach_text'); ?></p>
				<a href="<?php echo get_field('approach_link_url'); ?>" class="more dark"><span><?php echo get_field('approach_link_text'); ?></span><span class="tri"></span></a>
			</div>
		</article>
	</section>
	<section id="homeExp" class="block">
		<article class="contain cols">
		   <div class="row colFlex">
		      <div class="span1 buffer"></div>
		      <div class="span4 cont">
					<h2 class="stack"><?php echo get_field('blue_section_heading'); ?></h2>
					<p class="sm"><?php echo get_field('blue_section_text'); ?></p>
					<a href="<?php echo get_field('blue_section_link_url'); ?>" class="more hidden-sm"><span><?php echo get_field('blue_section_link_text'); ?></span><span class="tri"></span></a>
				</div>
		      <div class="span1"></div>
		      <ul class="span6 industries">
					<?php $industries = get_field('blue_section_list'); 
							foreach($industries as $industry) {
									echo "<li>".$industry['list_item']."</li>";
								}?>
				</ul>
				<a href="<?php echo get_field('blue_section_link_url'); ?>" class="more visible-sm"><span><?php echo get_field('blue_section_link_text'); ?></span><span class="tri"></span></a>
		   </div>
		</article>
	</section>

	<section id="profCar" class="splits block cycle-slideshow" data-cycle-fx="scrollHorz" 
    data-cycle-timeout="7000"
    data-cycle-slides="> article"
    data-cycle-prev=".cycle-prev"
    data-cycle-next=".cycle-next"
    data-cycle-pager="#custom-pager"     
    data-cycle-fx="fadeout"
    data-cycle-pager-template="<li><a href=#> 0{{slideNum}} </a></li>"
    data-cycle-caption=".capt-count"
    data-cycle-caption-template="0{{slideNum}}/0{{slideCount}}"
    >
    	<?php 
    	$args = array('post_type' => 'portfolio_companies',
					    'posts_per_page' => -1,);
		$profiles = new wp_query($args); 
		$slideCount = 1;
		while($profiles->have_posts()): $profiles->the_post();
		?>
			<article class="slide block slide<?php echo $slideCount; ?>">
			<div class="content">
				<div class="wrap">
					<h4><span class="count capt-count"></span><span><?php echo the_title(); ?></span></h4>
					<h2><?php echo get_field('slide_heading'); ?></h2>
					<p><?php echo the_excerpt(); ?></p>
					<a href="<?php echo the_permalink(); ?>" class="more dark"><span>Learn More</span><span class="tri"></span></a>
				</div>
			</div>
			<div class="img">
				<span style="background-image: url('<?php echo get_field('slide_image'); ?>');"></span>
			</div>
			</article>
			<?php $slideCount++; ?>
	<?php endwhile;
		wp_reset_query();
		?>
    	<!--
		<article class="slide block slide1">
			<div class="content">
				<div class="wrap">
					<h4><span class="count capt-count"></span><span>Gas Station TV</span></h4>
					<h2>Bringing Engagement To Your Morning Commute</h2>
					<p>Targeting the hard to reach, on-the–go consumer, GSTV delivers one-on-one exposure to hundreds of millions of viewers every month at the nation’s leading gas retailers across the United States.</p>
					<a href="#" class="more dark"><span>Learn More</span><span class="tri"></span></a>
				</div>
			</div>
			<div class="img">
				<span></span>
			</div>
		</article>
		<article class="slide block slide2">
			<div class="content">
				<div class="wrap">
					<h4><span class="count capt-count"></span><span>Connect America</span></h4>
					<h2>Providing Peace of Mind for Those Who Need it Most</h2>
					<p>As the largest independent medical alert company in North America, Connect America serves over 250,000 seniors and their loved ones every day - allowing them to continue to live independently and comfortably in their own homes.</p>
					<a href="#" class="more dark"><span>Learn More</span><span class="tri"></span></a>
				</div>
			</div>
			<div class="img">
				<span></span>
			</div>
		</article>
		<article class="slide block slide3">
			<div class="content">
				<div class="wrap">
					<h4><span class="count capt-count"></span><span>Robb Report</span></h4>
					<h2>The Manual of Modern Luxury</h2>
					<p>Widely regarded as the international authority on the luxury lifestyle, Robb Report is synonymous around the world with affluence, heritage, and the best of the best.</p>
					<a href="#" class="more dark"><span>Learn More</span><span class="tri"></span></a>
				</div>
			</div>
			<div class="img">
				<span></span>
			</div>
		</article>-->
		<ul id="custom-pager" class="pag cycle-pager">
			<!--<li class="active"><a href="#">01</a></li>
			<li><a href="#">02</a></li>
			<li><a href="#">03</a></li>-->
		</ul>
		<ul class="nav">
			<li class="next cycle-next"><a href="#"><span>Next</span></a></li>
			<li class="prev cycle-prev"><a href="#"><span>Previous</span></a></li>
		</ul>
	</section>
	<section class="testimonial-wrapper">
		<section id="testCar" class="block cycle-slideshow"  
		    data-cycle-timeout="7000"
		    data-cycle-slides="> article"
		    data-cycle-pager=".testiPag"
		    data-cycle-pager-template="<li><a href=#> 0{{slideNum}} </a></li>"
		    data-cycle-fx="scrollVert"
		    data-cycle-auto-height="calc">
		    	<?php
		    		$args = array('post_type' => 'testimonial', 'posts_per_page' => 3);
		    		$testimonials = new wp_query($args); 
		    		while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
		    		<article class="contain">
						<p class="quote"><?php echo get_field('testimonial', $testimonials->ID); ?></p>
						<p class="att">
							<span class="name"><?php echo get_field('author', $testimonials->ID); ?>, <?php echo get_field('title', $testimonials->ID); ?></span>
							<span class="company"><?php echo get_field('company', $testimonials->ID); ?></span>
						</p>
					</article>
					<?php endwhile;
					wp_reset_query(); ?>
		    	
			<ul class="pag testiPag">
				
			</ul>
		</section>
	</section>
	<section id="cultureBl" class="splits block">
		<div class="content">
			<div class="wrap">
				<h4><?php echo get_field('bottom_section_small_heading'); ?></h4>
				<h2><?php echo get_field('bottom_section_heading'); ?></h2>
				<p><?php echo get_field('bottom_section_text'); ?></p>
				<a href="<?php echo get_field('bottom_section_link_url'); ?>" class="more dark"><span><?php echo get_field('bottom_section_link_text'); ?></span><span class="tri"></span></a>
			</div>
		</div>
		<div class="img">
			<span style="background-image: url('<?php echo get_field('bottom_section_image'); ?>');"></span>
		</div>
	</section>

<?php get_footer(); ?>
