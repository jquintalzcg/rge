<?php /* Template Name: Contact Page Template */ get_header(); ?>
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
	?>
	<section id="contactForm">
		<h2 class="mobile"><?php echo the_title(); ?></h2>
		<div class="form">
			<h2 class="desktop"><?php echo the_title(); ?></h2>
			<?php the_content(); ?>
		</div>
		<div class="contacts">
			<h3>Contact</h3>
			<a href="tel:+1<?php echo get_field('phone'); ?>"><i class="fa fa-phone"></i><?php echo get_field('phone'); ?></a>
			<a href="mailto:<?php echo get_field('email'); ?>"><i class="fa fa-envelope"></i><?php echo get_field('email'); ?></a>
			<h3>Location</h3>
			<?php echo get_field('address'); ?>
			<h3>Connect</h3>
			<a href="<?php echo get_field('linkedin_url'); ?>"><i class="fa fa-linkedin-square"></i>LinkedIn</a>
		</div>
		<div class="map" style='background-image:url("<?php echo get_field('map_image'); ?>");'></div>
	</section>
	<?
} // end while
} // end if
?>
<?php get_footer(); ?>