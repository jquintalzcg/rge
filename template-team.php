<?php /* Template Name: Team Page Template */ get_header(); 
	$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
			$thumb_url = $thumb_url_array[0]; ?>
	<section id="portHero" class="hero block" style="background-image:url('<?php echo $thumb_url; ?>');">
		<a href="/rockbridge/" id="logo" class="main">Rockbridge Growth Equity</a>
		<h1><?php echo the_title() ?></h1>
	</section>
	<section class="intro block">
		<article class="contain cols">
			<div class="row colFlex">
				<h3 class="span5"><?php echo get_field('intro_heading'); ?></h3>
				<div class="span7">
					<p class="intro"><?php echo get_field('intro_text'); ?></p>
					<p class="secondary"><?php echo get_field('intro_cta'); ?></p>
					</div>
			</div>
		</article>
	</section>
	<section class="team-quote splits block">
		<div class="img half">
			<span class="" style="background-image: url('<?php echo get_field('quote_image'); ?>');"></span>
		</div>
		<div class="content">
			<p class="quote"><?php echo get_field('quote_text'); ?></p>			
			<p class="att">
				<span class="name"><?php echo get_field('author'); ?>, <?php echo get_field('author_title'); ?></span>
				<span class="company"><?php echo get_field('author_company'); ?></span>
			</p>
	</section>
	<section class="block" id="teamGrid">
		
		
		<ul class="widecontain">
			<li class="double">
				<h3><?php echo get_field('team_intro'); ?></h3>
			</li>
			<?php
						// Get the 'Plans' post type
						$args = array(
						    'post_type' => 'team',
						    'posts_per_page' => -1,
						    'order' => 'ASC'
						);
						$loop1 = new WP_Query($args);

						while($loop1->have_posts()): $loop1->the_post(); ?>
							<li>
								<a href="#" class="get_team" data-postid="<?php echo get_the_ID(); ?>" >
								<span class="photo tall" style="background-image: url('<?php echo get_field('thumbnail'); ?>');"></span>
								<div>
									<span class="title"><?php echo get_field('title'); ?> &mdash;</span>
									<span class="name"><?php echo the_title(); ?></span>
								</div>
								</a>
							</li>
						<?php endwhile;
						wp_reset_query();
						?>
			
		</ul>
	</section>
	<section class="testimonial-wrapper">
		<section id="testCar" class="block cycle-slideshow"  
		    data-cycle-timeout="7000"
		    data-cycle-slides="> article"
		    data-cycle-pager=".testiPag"
		    data-cycle-pager-template="<li><a href=#> 0{{slideNum}} </a></li>"
		    data-cycle-fx="scrollVert"
		    data-cycle-auto-height="calc">

			<article class="contain">
				<p class="quote">"The Rockbridge partnership has allowed Connect America to achieve unparalleled success in the industry by helping to create a truly scalable and technology-enabled operational infrastructure"</p>
				<p class="att">
					<span class="name">Ken Gross, Founder &amp; Executive Chairman</span>
					<span class="company">Connect America</span>
				</p>
			</article>
			<article class="contain">
				<p class="quote">"The Rockbridge team brought the resources and capabilities to transform One Reverse into the largest company in the reverse mortgage space. The exceptional growth of our company would not have come to fruition without Rockbridge’s sponsorship."</p>
				<p class="att">
					<span class="name">Gregg Smith, President &amp; COO</span>
					<span class="company">One Reverse Mortgage</span>
				</p>
			</article>
			<article class="contain">
				<p class="quote">"Rockbridge Growth Equity has been a valuable strategic partner for Triad Retail Media. Throughout this partnership, we have been able to accelerate our growth, expand internationally, and position Triad as the global leader in digital retail media."</p>
				<p class="att">
					<span class="name">Roger Berdusco, CEO</span>
					<span class="company">Triad Retail Media</span>
				</p>
			</article>
			<ul class="pag testiPag">
				<!--<li class="active"><a href="#">01</a></li>
				<li><a href="#">02</a></li>
				<li><a href="#">03</a></li>-->
			</ul>
		</section>
	</section>

	<div id="slideOut">
		<header id="slide-top">
			<button id="slideCl">Close</button>
		</header>
		<div id="ajaxLoader">
			<div class="ajaxstatus">
			</div>
		</div>
		<div class="port">
		</div>
	</div>
<?php get_footer(); ?>
