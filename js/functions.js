/**FADE IN BODY**/

 $('body').fadeIn(600);

/**PRELOADER**/

$(window).load(function() {
	$('#status').fadeOut();
	$('#preloader').delay(350).fadeOut('slow');
	$('#portfolio').delay(350).css({'overflow':'visible'});
});

/****FAST CLICK****/

$(function() {
	FastClick.attach(document.body);
});

/**NAV SLIDE**/

  $('#navBtn').on('click', function(e){
    e.preventDefault();
    $("#nav-full").toggleClass('open');
    $("#nav-overlay").toggleClass('open');
    $("#nav-top").toggleClass('hidden');
    $('body').addClass('overlain');
    $('html').addClass('overlain');
  });

  $('#navCl').on('click', function(e){
    e.preventDefault();
    $("#nav-full").removeClass('open');
    $("#nav-overlay").removeClass('open');
    $("#nav-top").removeClass('hidden');
    $('body').removeClass('overlain');
    $('html').removeClass('overlain');
  });

  $('#nav-overlay').on('click', function(e){
    e.preventDefault();
    $("#nav-full").removeClass('open');
    $("#nav-overlay").removeClass('open');
    $("#nav-top").removeClass('hidden');
    $('body').removeClass('overlain');
    $('html').removeClass('overlain');
    $('#slideOut').removeClass('open');
    jQuery("#slideOut .port").html('');

  });
  $('#slideCl').on('click', function(e){
    e.preventDefault();
    $("#slideOut").removeClass('open');
    jQuery("#slideOut .port").html('');
    $('body').removeClass('overlain');
    $('html').removeClass('overlain');
    $("#nav-overlay").removeClass('open');

  });
  $('a.get_pc').each(function(){
    $(this).on('click', function(e){
      e.preventDefault();
      $('body').addClass('overlain');
      $('html').addClass('overlain');
      $('#slideOut').addClass('open');
      $("#nav-overlay").addClass('open');
  });});
   $(document).keydown(function(e){
   	if(e.keyCode == 27) {
         $("#nav-full").removeClass('open');
         $("#nav-overlay").removeClass('open');
         $("#nav-top").removeClass('hidden');
         $('body').removeClass('overlain');
         $('html').removeClass('overlain');
         $('#slideOut').removeClass('open');
         jQuery("#slideOut .port").html('');
   	}     
   });

/**SMOOTH ANCHORS**/

$(document).ready(function(){
	$('a[href="#homeIntro"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 500, 'swing');
	});
});

/**STICKY ITEMS**/

$(window).scroll(function () {
   $('#nav-top').toggleClass("small", ($(window).scrollTop() > 1));
   $('#logoFt').toggleClass("active", ($(window).scrollTop() > 80));
});
